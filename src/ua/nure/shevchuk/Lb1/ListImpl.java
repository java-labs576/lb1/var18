package ua.nure.shevchuk.Lb1;

import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Iterator;
import java.util.Collection;

class Node {
	protected Object data;
	protected Node next;
	protected Node prev;
}

public class ListImpl implements List {
	
	private Node firstEl;
	private Node lastNode;
	private int size;
	
	@Override
	public void clear() {
		Node currentNode = firstEl;
		while (currentNode != null) {
			Node toRemove = currentNode;
			currentNode = currentNode.next;
			toRemove.data = null;
			toRemove.prev = null;
			toRemove.next = null;
		}
		firstEl = null;
		lastNode = null;
		size = 0;
	}

	@Override
	public Object get(int i) {
		return null;
	}

	@Override
	public Object set(int i, Object o) {
		return null;
	}

	@Override
	public void add(int i, Object o) {

	}

	@Override
	public Object remove(int i) {
		return null;
	}

	@Override
	public int indexOf(Object o) {
		return 0;
	}

	@Override
	public int lastIndexOf(Object o) {
		return 0;
	}

	@Override
	public ListIterator listIterator() {
		return null;
	}

	@Override
	public ListIterator listIterator(int i) {
		return null;
	}

	@Override
	public List subList(int i, int i1) {
		return null;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public boolean contains(Object o) {
		return false;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append('[');
		if (firstEl != null) {
			Node i = firstEl;
			while (i.next != null) {
			sb.append(i.data).append(", ");
			i = i.next;
			}
			sb.append(i.data);
		}
		sb.append(']');
		return sb.toString();
	}

	public void addFirst(Object element) {
		Node newNode = new Node();
		newNode.data = element;
		newNode.prev = null;
		if (firstEl == null) {
			newNode.next = null;
			firstEl = newNode;
			lastNode = newNode;
		} else {
			Node secondNode = firstEl;
			newNode.next = secondNode;
			firstEl = newNode;
			secondNode.prev = newNode;
		}
		++size;
	}

	public void addLast(Object element) {
		if (lastNode == null) {
			Node first = new Node();
			first.data = element;
			first.next = null;
			first.prev = null;
			lastNode = first;
			firstEl = first;
		} else {
			Node newNode = new Node();
			newNode.data = element;
			newNode.prev = lastNode;
			newNode.next = null;
			Node closerToFirst = lastNode;
			closerToFirst.next = newNode;
			lastNode = newNode;
		}
		++size;
	}

	public void removeFirst() {
		if (null == firstEl) {
			throw new NoSuchElementException();
		} else if (firstEl == lastNode) {
			firstEl.data = null;
			firstEl = null;
			lastNode = null;
		} else {
			Node second = firstEl.next;
			firstEl.data = null;
			firstEl.next = null;
			second.prev = null;
			firstEl = second;
		}
		--size;
	}

	public void removeLast() {
		if (null == firstEl && null == lastNode) {
			throw new NoSuchElementException();
		} else if (firstEl == lastNode) {
			firstEl.data = null;
			firstEl = null;
			lastNode = null;
		} else {
			Node penultimate = lastNode.prev;
			lastNode.data = null;
			lastNode.prev = null;
			penultimate.next = null;
			lastNode = penultimate;
		}
		--size;
	}

	public Object getFirst() {
		return firstEl == null ? null : firstEl.data;
	}

	public Object getLast() {
		return lastNode == null ? null : lastNode.data;
	}

	public Object search(Object element) {
		if (element == null) {
			return null;
		}
		for (Object i : this) {
			if (element.equals(i)) {
				return i;
			}
		}
		return null;
	}

	@Override
	public boolean remove(Object element) {
		
		Iterator<Object> it = this.iterator();
		if (element == null) {
			for (Object i = it.next(); it.hasNext(); i = it.next()) {
				if (null == i) {
					it.remove();
					return true;
				}
			}
		} else {
			for (Object i = it.next(); it.hasNext(); i = it.next()) {
				if (element.equals(i)) {
					it.remove();
					return true;
				}
			}
		}
		return false;
	}

	public boolean addAll(Collection collection) {
		return false;
	}

	public boolean addAll(int i, Collection collection) {
		return false;
	}

	public boolean retainAll(Collection collection) {
		return false;
	}

	public boolean removeAll(Collection collection) {
		return false;
	}

	public boolean containsAll(Collection collection) {
		return false;
	}

	@Override
	public Iterator<Object> iterator() {
		return new IteratorImpl();
	}

	@Override
	public Object[] toArray() {
		return new Object[0];
	}

	@Override
	public boolean add(Object o) {
		return false;
	}

	@Override
	public Object[] toArray(Object[] objects) {
		return new Object[0];
	}

	private class IteratorImpl implements Iterator<Object> {
		
		protected Node currentNode = firstEl;
		private boolean isAvailableRemove;
		
		@Override
		public boolean hasNext() {
			return null != currentNode;
		}
		
		void changeCurrentNode() {
			currentNode = currentNode.next;
		}
		
		@Override
		public Object next() {
			if (currentNode == null) {
				throw new NoSuchElementException();
			} else {
				Object toOutput = currentNode.data;
				changeCurrentNode();
				isAvailableRemove = true;
				return toOutput;
			}
		}
		
		void findCaseAndPerform() {
			Node nodeToRemove = null;
			if (currentNode == firstEl.next && lastNode != firstEl) {
					nodeToRemove = firstEl;
					currentNode.prev = null;
					nodeToRemove.prev = null;	
					firstEl = currentNode;
			} else if (currentNode == null && lastNode == firstEl) {
					nodeToRemove = firstEl;
					firstEl = null;
					lastNode = null;	
			} else if (currentNode == null) {
					nodeToRemove = lastNode;
					Node secondNode = nodeToRemove.prev;
					secondNode.next = null;
					lastNode = secondNode;
					nodeToRemove.prev = null;	
			} else {
					nodeToRemove = currentNode.prev;
					Node thirdNode = nodeToRemove.prev;
					currentNode.prev = thirdNode;
					thirdNode.next = currentNode;
					nodeToRemove.next = null;
					nodeToRemove.prev = null;
			}
			nodeToRemove.data = null;
		}
		
		@Override
		public void remove() {
			if (isAvailableRemove) {
				 findCaseAndPerform();
				--size;
			} else {
				throw new IllegalStateException();
			}
			isAvailableRemove = false;
		}
	}
	
	public Iterator<Object> anotherIterator() {
		return new AnotherIterator() ;
	}
	
	private final class AnotherIterator extends IteratorImpl {
		private AnotherIterator() {
			currentNode = lastNode;
		}
		
		@Override
		void changeCurrentNode() {
			currentNode = currentNode.prev;
		}
		
		@Override
		void findCaseAndPerform() {
			
			Node nodeToRemove = null;
			
			if (currentNode == null && lastNode == firstEl) {
					nodeToRemove = firstEl;
					firstEl = null;
					lastNode = null;
			} else if (currentNode == lastNode.prev && null != currentNode) {
					nodeToRemove = lastNode;
					currentNode.next = null;
					lastNode = currentNode;
					nodeToRemove.prev = null;
			} else if (currentNode == null) {
					nodeToRemove = firstEl;
					Node secondNode = nodeToRemove.next;
					secondNode.prev = null;
					firstEl = secondNode;
					nodeToRemove.next = null;	
			} else {
					nodeToRemove = currentNode.next;
					Node thirdNode = nodeToRemove.next;
					currentNode.next = thirdNode;
					thirdNode.prev = currentNode;
					nodeToRemove.next = null;
					nodeToRemove.prev = null;
			}
			
			nodeToRemove.data = null;
		}
	}
}
